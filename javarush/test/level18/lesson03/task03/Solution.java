package com.javarush.test.level18.lesson03.task03;

import java.io.BufferedReader;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* Самые частые байты
Ввести с консоли имя файла
Найти байты, которые чаше всех встречаются в файле
Вывести их на экран через пробел.
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        int s;
        String filename = reader.readLine();
        FileInputStream fr = new FileInputStream(filename);
        while (fr.available() > 0){
            s = fr.read();
            if(map.containsKey(s)){
                Integer v = map.get(s);
                map.remove(s);
            map.put(s,v+1);
            }
            else map.put(s, 1);
        }
        int a=0;
        for (int value : map.values()) {

          if(a<value) a = value;
        }

       /* for (Map.Entry entry : map.entrySet()) {
            if(Integer.parseInt(entry.getValue() + "") == a)            System.out.println( entry.getKey());D:\User\Desktop\1.txt
        }*/

        for (Map.Entry entry : map.entrySet()) {
            System.out.println("Key: " + entry.getKey() + " Value: "
                    + entry.getValue());
        }

        fr.close();
        reader.close();
    }
}
