package com.javarush.test.level18.lesson03.task05;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* Сортировка байт
Ввести с консоли имя файла
Считать все байты из файла.
Не учитывая повторений - отсортировать их по байт-коду в возрастающем порядке.
Вывести на экран
Закрыть поток ввода-вывода

Пример байт входного файла
44 83 44

Пример вывода
44 83
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        int s;
        String filename = reader.readLine();
        FileInputStream fr = new FileInputStream(filename);
        while (fr.available() > 0){
s=fr.read();
            map.put(s, 0);
        }

        List<Integer> list = new ArrayList(map.keySet());
        Collections.sort(list);



        for (Integer b: list){
            System.out.print(b+" ");

        }



        fr.close();
        reader.close();
    }
}
