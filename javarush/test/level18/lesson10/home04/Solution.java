package com.javarush.test.level18.lesson10.home04;

/* Объединение файлов
Считать с консоли 2 имени файла
В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов
Закрыть потоки
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fn1 = reader.readLine();
        String fn2 = reader.readLine();

        FileInputStream fis1 = new FileInputStream(fn1);
        FileInputStream fis2 = new FileInputStream(fn2);



        byte[] buffer1 = new byte[1024];
        byte[] buffer2 = new byte[1024];

        int count2 = fis2.read(buffer2);

       int count1 = fis1.read(buffer1);


        reader.close();
        fis2.close();
        fis1.close();

        FileOutputStream fos1 = new FileOutputStream(fn1);
        fos1.write(buffer2, 0, count2);
        fos1.write(buffer1, 0, count1);

        fos1.close();
    }
}
