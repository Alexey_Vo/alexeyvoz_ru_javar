package com.javarush.test.level18.lesson10.home03;

/* Два в одном
Считать с консоли 3 имени файла
Записать в первый файл содержимого второго файла, а потом дописать содержимое третьего файла
Закрыть потоки
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fn1 = reader.readLine();
        String fn2 = reader.readLine();
        String fn3 = reader.readLine();

        FileInputStream fis2 = new FileInputStream(fn2);
        FileInputStream fis3 = new FileInputStream(fn3);
        FileOutputStream fos1 = new FileOutputStream(fn1);

        byte[] buffer1= new byte[1024];
        byte[] buffer2 = new byte[1024];

        int count2 = fis2.read(buffer1);
        int count3 = fis3.read(buffer2);

        fos1.write(buffer1, 0, count2);
        fos1.write(buffer2, 0, count3);



        reader.close();
        fis2.close();
        fis3.close();
        fos1.close();

    }
}
