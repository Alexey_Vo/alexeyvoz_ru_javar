package com.javarush.test.level18.lesson10.home02;

/* Пробелы
В метод main первым параметром приходит имя файла.
Вывести на экран соотношение количества пробелов к количеству всех символов. Например, 10.45
1. Посчитать количество всех символов.
2. Посчитать количество пробелов.
3. Вывести на экран п2/п1*100, округлив до 2 знаков после запятой
4. Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

import java.util.ArrayList;
import java.util.List;

public class Solution {


    public static void main(String[] args) {
        List<Character> chars = new ArrayList<>();
        BufferedReader reader = null;
        double sumSpace = 0;
        try {


            reader = new BufferedReader(new FileReader(new File(args[0])));

            int c;

            while ((c = reader.read()) != -1) {
                chars.add((char) c);
                if (c == 32) sumSpace++;
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        double d = sumSpace*100/chars.size();
        String formattedDouble = String.format("%.2f", d);

        System.out.println(formattedDouble);

    }
}
