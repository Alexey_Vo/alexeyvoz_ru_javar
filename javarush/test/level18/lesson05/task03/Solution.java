package com.javarush.test.level18.lesson05.task03;

/* Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать бОльшую часть.
Закрыть потоки ввода-вывода
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fn1, fn2, fn3;

        fn1 = reader.readLine();
        fn2 = reader.readLine();
        fn3 = reader.readLine();

        FileInputStream inputStream = new FileInputStream(fn1);
        FileOutputStream outputStream2 = new FileOutputStream(fn2);
        FileOutputStream outputStream3 = new FileOutputStream(fn3);

        byte[] buffer3 = new byte[inputStream.available()/2];
        byte[] buffer2 = new byte[inputStream.available()-buffer3.length];

      outputStream2.write(buffer2,0, inputStream.read(buffer2));
      outputStream3.write(buffer3,0, inputStream.read(buffer3));

        reader.close();
        inputStream.close();
        outputStream2.close();
        outputStream3.close();




    }
}
