package com.javarush.test.level18.lesson05.task04;

/* Реверс файла
Считать с консоли 2 имени файла: файл1, файл2.
Записать в файл2 все байты из файл1, но в обратном порядке
Закрыть потоки ввода-вывода
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fn1, fn2;
        fn1 = reader.readLine();
        fn2 = reader.readLine();
        FileInputStream fin1 = new FileInputStream(fn1);
        FileOutputStream fon2 = new FileOutputStream(fn2);
        if (fin1.available() > 0) {
            //читаем весь файл одним куском
            byte[] buffer = new byte[fin1.available()];
            byte[] buffer2 = new byte[fin1.available()];

            int count = fin1.read(buffer);

            for (int i = buffer.length-1; i >= 0; i--) {
                buffer2[i]=buffer[buffer.length-1-i];
            }

            fon2.write(buffer2, 0, count);
        }
        reader.close();
        fin1.close();
        fon2.close();
    }
}
