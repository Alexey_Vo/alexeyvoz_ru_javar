package com.javarush.test.level25.lesson07.home01;

public class TaskManipulator implements Runnable, CustomThreadManipulator {

    Thread current = Thread.currentThread();
    boolean go = true;
    Thread eta ;

    @Override
    public void run() {
        try {
            Thread.sleep(0);

            while (!eta.isInterrupted()) {

                System.out.println(eta.getName());
                Thread.sleep(90);

            }
        }
          catch (InterruptedException e){}


        }



    @Override
    public void start(String threadName) {

        eta = new Thread(this);
        eta.setName(threadName);
        eta.start();

    }

    @Override
    public void stop(){

       eta.interrupt();

    }
}
