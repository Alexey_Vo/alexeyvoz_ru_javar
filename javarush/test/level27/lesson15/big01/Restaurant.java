package com.javarush.test.level27.lesson15.big01;

import com.javarush.test.level27.lesson15.big01.kitchen.Cook;
import com.javarush.test.level27.lesson15.big01.kitchen.Waitor;

/**
 * Created by User on 17.06.2016.
 */
public class Restaurant {

    public static void main(String[] args) {
        Tablet tablet = new Tablet(5);
        tablet.createOrder();
        Cook cook = new Cook("Amigo");
        Waitor waitor = new Waitor();
        if(!tablet.currentOrder.isEmpty()) {
            cook.update(tablet, tablet.currentOrder);
            waitor.update(cook, tablet.currentOrder);
        }

    }
}
