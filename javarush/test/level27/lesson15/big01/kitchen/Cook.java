package com.javarush.test.level27.lesson15.big01.kitchen;

import com.javarush.test.level27.lesson15.big01.ConsoleHelper;



import java.util.Observable;
import java.util.Observer;

/**
 * Created by User on 21.06.2016.
 */
public class Cook extends Observable implements Observer {
    public String name;


    public Cook(String name) {
        this.name = name;

    }

    @Override
    public String toString() {
        return name;
    }


    @Override
    public void update(Observable o, Object arg) {
        Order ord = (Order)arg;
        ConsoleHelper.writeMessage("Start cooking - "+ arg+ ", cooking time " + ord.getTotalCookingTime() + "min");

        setChanged();
        notifyObservers(arg);
    }
}
