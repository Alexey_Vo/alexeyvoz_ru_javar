package com.javarush.test.level27.lesson15.big01.kitchen;

import com.javarush.test.level27.lesson15.big01.ConsoleHelper;
import com.javarush.test.level27.lesson15.big01.Tablet;


import java.io.IOException;
import java.util.List;


/**
 * Created by User on 17.06.2016.
 */
public class Order {
  private List<Dish> dishes;
  private   Tablet tablet;


    public Order( Tablet t) throws IOException{
        this.dishes = ConsoleHelper.getAllDishesForOrder();
        tablet = t;
    }

    @Override
    public String toString() {
        if(dishes.isEmpty()) return "";
        else return "Your order: "+dishes.toString()+" of Tablet{number="+tablet.number+"}";
    }

    public boolean isEmpty(){
        return dishes.isEmpty();
    }

    public int getTotalCookingTime(){
        int sum = 0;
        for (Dish x : dishes) sum = sum + x.getDuration();

        return sum;
    }
}
