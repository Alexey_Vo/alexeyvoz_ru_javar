package com.javarush.test.level27.lesson15.big01.ad;


import com.javarush.test.level27.lesson15.big01.ConsoleHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by User on 22.06.2016.
 */
public class AdvertisementManager {
   private final AdvertisementStorage storage = AdvertisementStorage.getInstance();
   private int timeSecond;
   public AdvertisementManager(int timeSeconds) {
        this.timeSecond = timeSeconds;
    }

    public void processVideos(){
//        List<Advertisement> list = new ArrayList<>();
//        List<Advertisement> tempList = new ArrayList<>();
//        long sum = 0;
//        long tempSum = 0;
//        int t = timeSecond;
//        for (Advertisement ad : storage.list()) {
//
//            t = t - ad.getDuration();
//            if(t<0) if(tempSum>sum) {sum = tempSum;
//                list = tempList;}
//            else {
//                tempSum = tempSum + ad.getAmountPerOneDisplaying();
//                list.add(ad);
//            }
//
//        }














        {Collections.sort(storage.list(), new Comparator<Advertisement>() {
            @Override
            public int compare(Advertisement o1, Advertisement o2) {
                if((int)(o1.getAmountPerOneDisplaying()-o2.getAmountPerOneDisplaying()) == 0)
                    return (int)(o2.getAmountPerOneDisplaying()*1000/o2.getDuration() - o1.getAmountPerOneDisplaying()*1000/o1.getDuration());
                else return (int)(o1.getAmountPerOneDisplaying()-o2.getAmountPerOneDisplaying());
            }
        });
        }
         int timeleft = timeSecond;
        for (Advertisement ad : storage.list()) {
            if (timeleft < ad.getDuration()) continue;
            ConsoleHelper.writeMessage(ad.getName()+ " is displaying..."+ad.getAmountPerOneDisplaying()+" "+ ad.getAmountPerOneDisplaying()*1000/ad.getDuration() );
            timeleft-=ad.getDuration();
            ad.revalidate();
        }
        if (timeleft == timeSecond) {
            throw new NoVideoAvailableException();
        }


    }
}
