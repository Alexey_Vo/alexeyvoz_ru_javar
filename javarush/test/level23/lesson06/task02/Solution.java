package com.javarush.test.level23.lesson06.task02;

/* Рефакторинг
Отрефакторите класс Solution: вынесите все константы в public вложенный(nested) класс Constants.
Запретите наследоваться от Constants.
*/
public class Solution {

    final public static class Constants{
        public static final String s1 = "Server is not accessible for now.";
        public static final String s2 = "User is not authorized.";
        public static final String s3 = "User is banned.";
        public static final String s4 = "Access is denied.";
    }

    public class ServerNotAccessibleException extends Exception {
        public ServerNotAccessibleException() {
            super(Solution.Constants.s1);
        }

        public ServerNotAccessibleException(Throwable cause) {
            super(Solution.Constants.s1, cause);
        }
    }

    public class UnauthorizedUserException extends Exception {
        public UnauthorizedUserException() {
            super(Solution.Constants.s2);
        }

        public UnauthorizedUserException(Throwable cause) {
            super(Solution.Constants.s2, cause);
        }
    }

    public class BannedUserException extends Exception {
        public BannedUserException() {
            super(Solution.Constants.s3);
        }

        public BannedUserException(Throwable cause) {
            super(Solution.Constants.s3, cause);
        }
    }

    public class RestrictionException extends Exception {
        public RestrictionException() {
            super(Solution.Constants.s4);
        }

        public RestrictionException(Throwable cause) {
            super(Solution.Constants.s4, cause);
        }
    }
}
