package com.javarush.test.level08.lesson11.home09;


import java.util.Date;

/* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true, если количество дней с начала года - нечетное число, иначе false
2. String date передается в формате MAY 1 2013
Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false
*/

public class Solution
{
    public static void main(String[] args)
    {
       isDateOdd("JANUARY 1 2000");
    }

    public static boolean isDateOdd(String date)
    {
        Date d1 = new Date();
        Date d2 = new Date(date);



        d1.setYear(d2.getYear());
        d1.setMonth(0);
        d1.setDate(1);

        long timeDistms = d2.getTime() - d1.getTime();

        int timeDistDay = (int) (timeDistms/(1000*24*60*60));
        if(timeDistDay%2 != 0) return true;
        else return false;
    }
}
