package com.javarush.test.level08.lesson11.home05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        char[] ch = s.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            int a = s.indexOf(" ", i);

            if (a == -1) break;
            else {
                ch[a + 1] = Character.toUpperCase(ch[a + 1]);
                if (i == 0) ch[0] = Character.toUpperCase(ch[0]);
                s = new String(ch);
                }
        }
        System.out.println(s);

    }
}
