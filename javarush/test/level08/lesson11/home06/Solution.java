package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<Human> chiDed1 = new ArrayList<Human>();
        Human ch1 = new Human("Kolya", true, 21, new ArrayList<Human>());
        Human ch2 = new Human("Nastya", false, 18, new ArrayList<Human>());
        Human ch3 = new Human("Sasha", false, 15, new ArrayList<Human>());
        ArrayList<Human> st1 = new ArrayList<Human>();
        st1.add(ch1);
        st1.add(ch2);
        st1.add(ch3);
        Human father = new Human("Arsen", true, 51, st1);
        Human mother = new Human("Nadya", false, 44, st1);
        ArrayList<Human> st2a = new ArrayList<Human>();
        ArrayList<Human> st2b = new ArrayList<Human>();
        st2a.add(father);
        st2b.add(mother);

        Human ded1 = new Human("Igor", true, 78, st2b);
        Human ded2 = new Human("Ashot", true, 83, st2b);
        Human babka1 = new Human("Marya", false, 74, st2a);
        Human babka2 = new Human("Tamara", false, 81, st2a);


        System.out.println(ded1);
        System.out.println(ded2);
        System.out.println(babka1);
        System.out.println(babka2);
        System.out.println(father);
        System.out.println(mother);
        System.out.println(ch1);
        System.out.println(ch2);
        System.out.println(ch3);
    }

    public static class Human
    {
        String name;
        boolean sex;
        int age = 50;
        ArrayList<Human> children = new ArrayList<Human>();

        public Human(String name, boolean sex, int age, ArrayList<Human> list){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children =  list;
        }
        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}
