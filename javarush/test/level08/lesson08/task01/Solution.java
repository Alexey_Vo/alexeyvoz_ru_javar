package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {
      HashSet<String> set = new HashSet<String>();
        set.add("Ла");
        set.add("Ла2");
        set.add("Ла3");
        set.add("Л4");
        set.add("Ла5");
        set.add("Л6");
        set.add("Ла7");
        set.add("Ла8");
        set.add("Ла89");
        set.add("Ла344");
        set.add("Ла453");
        set.add("Ла56");
        set.add("Ла567");
        set.add("Ла578");
        set.add("Ла584");
        set.add("Ла44");
        set.add("Ла55");
        set.add("Ла66");
        set.add("Ла45");
        set.add("Ла3345665");
        return set;
    }
    Set<String> set1 = createSet();
}
