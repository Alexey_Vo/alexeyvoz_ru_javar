package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{
    public static void main(String[] args) throws Exception{

        HashMap<String,String> map = createMap();
        System.out.println(getCountTheSameFirstName(map, "Vasya5"));
        System.out.println(getCountTheSameLastName(map, "Petrov"));
    }

    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> map = new HashMap<String,String>();
        map.put("Petrov", "Vasya1");
        map.put("Fedorov", "Vasya2");
        map.put("Ivanov", "Vasya3");
        map.put("Petrov2", "Vasya4");
        map.put("Petrov3", "Vasya5");
        map.put("Petrov4", "Vasya5");
        map.put("Petrov5", "Vasya6");
        map.put("Petrov6", "Vasya7");
        map.put("Petrov7", "Vasya8");
        map.put("Petrov8", "Vasya5");
        return map;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        int i =0;
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, String> pair = iterator.next();
            String value = pair.getValue();
            if(value.equals(name))  i++;

        }
     return i;
    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String familiya)
    {
        int i =0;
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, String> pair = iterator.next();
            String key = pair.getKey();
            if(familiya.equals(key)) i++;

        }
        return i;
    }
}
