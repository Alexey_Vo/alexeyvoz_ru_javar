package com.javarush.test.level28.lesson06.home01;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by User on 29.06.2016.
 */
public class MyThread extends Thread {

    public static AtomicInteger priority = new AtomicInteger(0);

    public synchronized void counterThread(){
        priority.incrementAndGet();
        priority.compareAndSet(11,1);
        setPriority(priority.get());


    }
    public MyThread() {
        super();
        counterThread();
    }

    public MyThread(Runnable target) {
        super(target);
        counterThread();
    }

    public MyThread(ThreadGroup group, Runnable target) {
        super(group, target);
        counterThread();
    }

    public MyThread(String name) {
        super(name);
        counterThread();
    }

    public MyThread(ThreadGroup group, String name) {
        super(group, name);
        counterThread();
    }

    public MyThread(Runnable target, String name) {
        super(target, name);
        counterThread();
    }

    public MyThread(ThreadGroup group, Runnable target, String name) {
        super(group, target, name);
        counterThread();
    }

    public MyThread(ThreadGroup group, Runnable target, String name, long stackSize) {
        super(group, target, name, stackSize);
        counterThread();
    }
}
