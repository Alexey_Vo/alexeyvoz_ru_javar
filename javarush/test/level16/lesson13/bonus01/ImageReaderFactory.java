package com.javarush.test.level16.lesson13.bonus01;

import com.javarush.test.level16.lesson13.bonus01.common.*;

/**
 * Created by User on 14.05.2015.
 */
public class ImageReaderFactory {
    public static ImageReader getReader(ImageTypes a){
if(a == ImageTypes.BMP) return new BmpReader();
        else if(a == ImageTypes.JPG) return new JpgReader();
        else if(a == ImageTypes.PNG) return new PngReader();
        else throw new IllegalArgumentException();
    }
}
