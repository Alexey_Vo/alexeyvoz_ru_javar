package com.javarush.test.level14.lesson08.home09;

/**
 * Created by User on 17.04.2015.
 */
public class Hrivna extends Money {
    @Override
    public String getCurrencyName() {
        return "HRN";
    }

    public Hrivna(double amount) {
        super(amount);
    }

    @Override
    public double getAmount() {
        return super.getAmount();
    }
}
