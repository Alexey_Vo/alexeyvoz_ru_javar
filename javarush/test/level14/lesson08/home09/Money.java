package com.javarush.test.level14.lesson08.home09;

public abstract class Money
{
    private double a;
    public Money(double amount)
    {
        this.a = amount;
    }


    public  double getAmount(){
        return a;
    }

    public abstract String getCurrencyName();
}

