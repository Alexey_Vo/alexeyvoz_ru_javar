package com.javarush.test.level14.lesson08.bonus03;

/**
 * Created by User on 18.04.2015.
 */
public class Singleton {
    static Singleton sin = null;

    static Singleton getInstance(){
        if(sin == null) sin = new Singleton();
        return Singleton.sin;
    }

    private Singleton() {

    }
}
