package com.javarush.test.level14.lesson08.home01;

/**
 * Created by User on 14.04.2015.
 */
public class WaterBridge implements Bridge {
    @Override
    public int getCarsCount() {
        return 1;
    }
}
