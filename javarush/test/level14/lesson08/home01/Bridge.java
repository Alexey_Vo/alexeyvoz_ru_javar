package com.javarush.test.level14.lesson08.home01;

/**
 * Created by User on 14.04.2015.
 */
public interface Bridge {
    int getCarsCount();
}
