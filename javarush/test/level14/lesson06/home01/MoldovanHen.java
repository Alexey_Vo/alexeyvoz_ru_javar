package com.javarush.test.level14.lesson06.home01;

/**
 * Created by User on 14.04.2015.
 */
public class MoldovanHen extends Hen implements Country {
    @Override
    int getCountOfEggsPerMonth() {
        return 6;
    }

    @Override
    public String getDescription() {
        return super.getDescription()+ " Моя страна - "+MOLDOVA+". Я несу "+ this.getCountOfEggsPerMonth()+" яиц в месяц.";
    }
}
