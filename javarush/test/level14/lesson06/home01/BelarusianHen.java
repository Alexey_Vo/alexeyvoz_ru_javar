package com.javarush.test.level14.lesson06.home01;

/**
 * Created by User on 14.04.2015.
 */
public class BelarusianHen extends Hen implements Country {
    @Override
    int getCountOfEggsPerMonth() {
               return 5;
    }

    @Override
    public String getDescription() {
        return super.getDescription() + " Моя страна - "+BELARUS+". Я несу "+ this.getCountOfEggsPerMonth()+" яиц в месяц.";
    }
}
