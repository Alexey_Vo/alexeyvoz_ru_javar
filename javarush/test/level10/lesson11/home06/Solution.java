package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        String name;
        String name2;
        boolean sex = true;
        int age = 99;
        int weight = 70;
        int hight = 165;

        public Human(int hight) {
            this.hight = hight;
        }

        public Human() {
        }

        public Human(String name, String name2, boolean sex, int age) {
            this.name = name;
            this.name2 = name2;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, String name2){
           this.name = name;
           this.name2 = name;

    }
       public Human(String name, boolean sex){
           this.name = name;
           this.sex = sex;

    }
       public Human(String name, String name2, boolean sex){
           this.name = name;
           this.name2 = name2;
           this.sex = sex;
    }
       public Human(String name, int age){
           this.name = name;
           this.age = age;
    }
       public Human(boolean sex, int age){
           this.sex= sex;
           this.age = age;

    }
       public Human(int age, int weight){
           this.age = age;
           this.weight = weight;

    }
       public Human(int age, int weight, int hight){
           this.age = age;
           this.weight = weight;
           this.hight = hight;
    }
       public Human(String name, int weight, int hight){
           this.name = name;
           this.weight = weight;
           this.hight = hight;

    }
       public Human(String name,boolean sex, int weight, int hight){
           this.name = name;
           this.sex = sex;
           this.weight = weight;
           this.hight = hight;
    }
       public Human(String name, String name2, int age, boolean sex, int weight, int hight){
           this.name = name;
           this.name2 = name2;
           this.age = age;
           this.sex = sex;
           this.weight = weight;
           this.hight = hight;
    }

        public Human(String name, String name2, boolean sex, int age, int weight) {
            this.name = name;
            this.name2 = name2;
            this.sex = sex;
            this.age = age;
            this.weight = weight;
        }
    }
}
