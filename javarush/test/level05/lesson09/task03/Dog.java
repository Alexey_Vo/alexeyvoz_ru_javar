package com.javarush.test.level05.lesson09.task03;

/* Создать класс Dog
Создать класс Dog (собака) с тремя конструкторами:
- Имя
- Имя, рост
- Имя, рост, цвет
*/

public class Dog
{
    private String nameDog = null;
    private int heightDog = 999;
    private String colorDog = "hz";

    public Dog(String name){
        this.nameDog=name;
    }
    public Dog(String name, int height){
        this.nameDog=name;
        this.heightDog=height;
    }
    public Dog(String name, int height, String color){
        this.nameDog=name;
        this.heightDog=height;
        this.colorDog=color;
    }

}
