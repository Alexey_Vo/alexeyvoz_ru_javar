package com.javarush.test.level05.lesson09.task04;

/* Создать класс Circle
Создать класс (Circle) круг, с тремя конструкторами:
- centerX, centerY, radius
- centerX, centerY, radius, width
- centerX, centerY, radius, width, color
*/

public class Circle
{
    private int centerX = 0;
    private int centerY = 0;
    private int radius = 1;
    private int width = 1;
    private String color = "hz";

    public Circle(int center1, int center2, int radius0){
        this.centerX=center1;
        this.centerY=center2;
        this.radius=radius0;
    }
    public Circle(int center1, int center2, int radius0, int width0){
        this.centerX=center1;
        this.centerY=center2;
        this.radius=radius0;
        this.width=width0;
    }
    public Circle(int center1, int center2, int radius0, int width0, String color0){
        this.centerX=center1;
        this.centerY=center2;
        this.radius=radius0;
        this.width=width0;
        this.color=color0;
    }
}
