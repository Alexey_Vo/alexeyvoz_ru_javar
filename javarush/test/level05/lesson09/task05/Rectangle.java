package com.javarush.test.level05.lesson09.task05;

/* Создать класс прямоугольник (Rectangle)
Создать класс прямоугольник (Rectangle). Его данными будут top, left, width, height (левая координата, верхняя, ширина и высота). Создать для него как можно больше конструкторов:
Примеры:
-	заданы 4 параметра: left, top, width, height
-	ширина/высота не задана (оба равны 0)
-	высота не задана (равно ширине) создаём квадрат
-	создаём копию другого прямоугольника (он и передаётся в параметрах)
*/

public class Rectangle
{
    private int top = 0;
    private int left = 0;
    private int width = 0;
    private int height = 0;

    public Rectangle(int top1, int left1, int width1, int height1){
        this.top=top1;
        this.left=left1;
        this.width=width1;
        this.height=height1;
    }
    public Rectangle(int top1, int left1){
        this.left=left1;
        this.top=top1;
    }
    public Rectangle(int top1, int left1, int width1){
        this.left=left1;
        this.top=top1;
        this.width=width1;
        this.height=width1;
    }
    public Rectangle(Rectangle rex){
        this.top=rex.top;
        this.left=rex.left;
        this.width=rex.width;
        this.height=rex.height;
    }

}
