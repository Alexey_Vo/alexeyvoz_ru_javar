package com.javarush.test.level05.lesson09.task02;

/* Создать класс Cat
Создать класс Cat (кот) с пятью конструкторами:
- Имя,
- Имя, вес, возраст
- Имя, возраст (вес стандартный)
- вес, цвет, (имя, адрес и возраст – неизвестные. Кот - бездомный)
- вес, цвет, адрес ( чужой домашний кот)
Задача инициализатора – сделать объект валидным. Например, если вес не известен, то нужно указать какой-нибудь средний вес. Кот не может ничего не весить. То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.
*/

public class Cat
{
    private String nameCat = null;
    private int weightCat = 1;
    private int ageCat = 1;
    private String colorCat = "hz";
    private String adresCat = null;

    public Cat(String name){
        this.nameCat=name;
    }
    public Cat(String name, int weight, int age){
        this.nameCat=name;
        this.weightCat=weight;
        this.ageCat=age;
    }
    public Cat(String name,int age){
        this.nameCat=name;
        this.ageCat=age;
    }
    public Cat(int age, String color){
        this.ageCat=age;
        this.colorCat=color;
    }
    public Cat(int weight, String color, String adres){
        this.weightCat=weight;
        this.colorCat=color;
        this.adresCat=adres;
    }
}
