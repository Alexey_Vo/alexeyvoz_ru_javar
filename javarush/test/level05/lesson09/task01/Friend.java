package com.javarush.test.level05.lesson09.task01;

/* Создать класс Friend
Создать класс Friend (друг) с тремя конструкторами:
- Имя
- Имя, возраст
- Имя, возраст, пол
*/

public class Friend
{
    private String nameFriend = "Ara";
    private int ageFriend = 99;
    private String sexFriend = "unisex";

    public Friend(String name){
        this.nameFriend=name;
    }

    public Friend(String name, int age){
        this.nameFriend=name;
        this.ageFriend=age;
    }
    public Friend(String name, int age, String sex){
        this.nameFriend=name;
        this.ageFriend=age;
        this.sexFriend=sex;
    }

}