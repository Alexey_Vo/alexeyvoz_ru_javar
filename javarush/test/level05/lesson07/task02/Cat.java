package com.javarush.test.level05.lesson07.task02;

/* Создать класс Cat
Создать класс Cat (кот) с пятью инициализаторами:
- Имя,
- Имя, вес, возраст
- Имя, возраст (вес стандартный)
- вес, цвет, (имя, адрес и возраст неизвестны, это бездомный кот)
- вес, цвет, адрес ( чужой домашний кот)
Задача инициализатора – сделать объект валидным. Например, если вес неизвестен, то нужно указать какой-нибудь средний вес. Кот не может ничего не весить. То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.
*/

public class Cat
{
    private String nameCat = null;
    private int weightCat = 1;
    private int ageCat = 1;
    private String colorCat = null;
    private String adresCat = null;

    public void initialize(String name){
        this.nameCat = name;
    }
    public void initialize(String name, int weight, int age){
        this.nameCat = name;
        this.weightCat = weight;
        this.ageCat = age;
    }
    public void initialize(String name, int age){
        this.nameCat = name;
        this.ageCat = age;
    }
    public void initialize(int weight, String color){
        this.weightCat = weight;
        this.colorCat = color;
    }
    public void initialize(int weigh, String color, String adres){
        this.weightCat = weigh;
        this.colorCat = color;
        this.adresCat = adres;
    }
}
