package com.javarush.test.level05.lesson07.task01;

/* Создать класс Friend
Создать класс Friend (друг) с тремя инициализаторами (тремя методами initialize):
- Имя
- Имя, возраст
- Имя, возраст, пол
*/

public class Friend
{
    private String name = null;
    private int age = 0;
    private String sex = null;
    public void initialize(String iname){
        this.name= iname;
    }
    public void initialize(String iname,int iage){
        this.name=iname;
        this.age=iage;
    }
    public void initialize(String iname, int iage, String isex){
    this.name=iname;
    this.age=iage;
    this.sex=isex;
    }

}
