package com.javarush.test.level05.lesson07.task05;

/* Создать класс прямоугольник (Rectangle)
Создать класс прямоугольник (Rectangle). Его данными будут top, left, width, height (левая координата, верхняя, ширина и высота). Создать для него как можно больше методов initialize(…)
Примеры:
-	заданы 4 параметра: left, top, width, height
-	ширина/высота не задана (оба равны 0)
-	высота не задана (равно ширине) создаём квадрат
-	создаём копию другого прямоугольника (он и передаётся в параметрах)
*/

public class Rectangle
{
    private int top = 0;
    private int left = 0;
    private int widht = 0;
    private int height = 0;

    public void initialize(int left1, int top1, int widht1, int height1){
        this.left=left1;
        this.top=top1;
        this.widht=widht1;
        this.height=height1;
    }
    public void initialize(int left1, int top1){
        this.left=left1;
        this.top=top1;
    }
    public void initialize(int left1, int top1, int widht1){
        this.left=left1;
        this.top=top1;
        this.widht=widht1;
        this.height=widht1;
    }
    public void initialize(Rectangle rex){
        this.top=rex.top;
        this.left=rex.left;
        this.height=rex.height;
        this.widht=rex.widht;
    }



}
