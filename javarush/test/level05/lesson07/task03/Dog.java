package com.javarush.test.level05.lesson07.task03;

/* Создать класс Dog
Создать класс Dog (собака) с тремя инициализаторами:
- Имя
- Имя, рост
- Имя, рост, цвет
*/

public class Dog
{
    private String nameDog = null;
    private int hightDog = 100;
    private String colorDog = "hz";

    public void initialize(String name){

        this.nameDog=name;
    }

    public void initialize(String name, int hight){
        this.nameDog=name;
        this.hightDog=hight;
    }

    public void initialize(String name, int hight, String color){
        this.nameDog=name;
        this.hightDog=hight;
        this.colorDog=color;
    }

}
