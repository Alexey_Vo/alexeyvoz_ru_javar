package com.javarush.test.level05.lesson07.task04;

/* Создать класс Circle
Создать класс (Circle) круг, с тремя инициализаторами:
- centerX, centerY, radius
- centerX, centerY, radius, width
- centerX, centerY, radius, width, color
*/

public class Circle
{
    private int centerX = 0;
    private int centerY = 0;
    private int radius = 1;
    private int widht = 1;
    private String color = "hz";


    public void initialize(int center1, int center2, int radius1){
        this.centerX=center1;
        this.centerY=center2;
        this.radius=radius1;
    }
    public void initialize(int center1, int center2, int radius1, int widht1){
        this.centerX=center1;
        this.centerY=center2;
        this.radius=radius1;
        this.widht=widht1;
    }
    public void initialize(int center1, int center2, int radius1, int widht1, String color1){
        this.centerX=center1;
        this.centerY=center2;
        this.radius=radius1;
        this.widht=widht1;
        this.color=color1;

    }




}
