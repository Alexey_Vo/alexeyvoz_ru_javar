package com.javarush.test.level05.lesson12.home03;

/* Создай классы Dog, Cat, Mouse
Создай классы Dog, Cat, Mouse. Добавь по три поля в каждый класс, на твой выбор. Создай объекты для героев мультика Том и Джерри. Так много, как только вспомнишь.
Пример:
Mouse jerryMouse = new Mouse(“Jerry”, 12 , 5), где 12 - высота в см, 5 - длина хвоста в см.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Mouse jerryMouse = new Mouse("Jerry", 12 , 5);

        Dog dog1 = new Dog("Spike", 15, 8);

        Cat cat1 = new Cat("Tom", 30, 7);

        Cat cat2 = new Cat("Jusper", 35, 9 );
    }

    public static class Mouse
    {
        String name;
        int height;
        int tail;

        public Mouse(String name, int height, int tail)
        {
            this.name = name;
            this.height = height;
            this.tail = tail;
        }
    }

    public static class Dog{
        String name;
        int weight;
        int age;

        public Dog(String name1, int weight1, int age1){
            this.name=name1;
            this.weight=weight1;
            this.age=age1;
        }

    }
    public static class Cat{
        String name;
        int height;
        int age;

        public Cat(String name1, int height1, int age1){
            this.name=name1;
            this.height=height1;
            this.age=age1;
        }
    }

    //Напишите тут ваши классы

}
