package com.javarush.test.level05.lesson12.home05;

/* Вводить с клавиатуры числа и считать их сумму
Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма». Вывести на экран полученную сумму.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        boolean a = true;
        int i = 0;
        int sum = 0;
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));

        while(a)
        {
            a = true;
            String b = r.readLine();

            if(b.equals("сумма")) {
                a = false;
                break;
            }
            else
            i = Integer.parseInt(b);


            sum=sum + i;


        }
        System.out.println(sum);
    }
}



