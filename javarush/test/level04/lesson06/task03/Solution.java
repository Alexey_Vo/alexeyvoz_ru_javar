package com.javarush.test.level04.lesson06.task03;

/* Сортировка трех чисел
Ввести с клавиатуры три числа, и вывести их в порядке убывания.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String a1 = reader.readLine();
        int a = Integer.parseInt(a1);

        String b1 = reader.readLine();
        int b = Integer.parseInt(b1);

        String c1 = reader.readLine();
        int c = Integer.parseInt(c1);

        if(a<b && c < b ) {
            System.out.println(b);
            if(a<c){
                System.out.println(c);
                System.out.println(a);
            }
            else {
                System.out.println(a);
                System.out.println(c);
            }
        }

        if(a<c && b < c ) {
            System.out.println(c);
            if(a<b){
                System.out.println(b);
                System.out.println(a);
            }
            else {
                System.out.println(a);
                System.out.println(b);
            }
        }

        if(b<a && c < a ) {
            System.out.println(a);
            if(b<c){
                System.out.println(c);
                System.out.println(b);
            }
            else {
                System.out.println(b);
                System.out.println(c);
            }
        }
    }

}
