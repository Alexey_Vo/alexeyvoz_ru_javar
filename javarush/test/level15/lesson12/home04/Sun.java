package com.javarush.test.level15.lesson12.home04;

/**
 * Created by User on 24.04.2015.
 */
public class Sun implements Planet{
    private static Sun instanceSun;

    public static Sun getInstance() {
        if(instanceSun==null) instanceSun = new Sun();
        return instanceSun;
    }

    private Sun() {
    }
}
