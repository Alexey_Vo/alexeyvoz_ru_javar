package com.javarush.test.level15.lesson12.home05;

/**
 * Created by User on 24.04.2015.
 */
public class SubSolution extends Solution {
    public SubSolution(int a) {
        super(a);
    }

    public SubSolution(String n) {
        super(n);
    }

    public SubSolution() {
    }

    protected SubSolution(String a, String b, String c) {
        super(a, b, c);
    }

    protected SubSolution(int a, int b, int c) {
        super(a, b, c);
    }

    protected SubSolution(short a) {
        super(a);
    }

     SubSolution(String a, String b, String c, String d) {
        super(a, b, c, d);
    }

     SubSolution(int a, int b, int c, int d) {
        super(a, b, c, d);
    }

     SubSolution(long l) {
        super(l);
    }
    private SubSolution(String a, String b){}
    private SubSolution(int a, int b){}
    private SubSolution(double d){}
}
