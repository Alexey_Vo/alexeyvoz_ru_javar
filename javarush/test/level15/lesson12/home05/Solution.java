package com.javarush.test.level15.lesson12.home05;

/* Перегрузка конструкторов
1. В классе Solution создайте по 3 конструктора для каждого модификатора доступа.
2. В отдельном файле унаследуйте класс SubSolution от класса Solution.
3. Внутри класса SubSolution создайте конструкторы командой Alt+Insert -> Constructors.
4. Исправьте модификаторы доступа конструкторов в SubSolution так, чтобы они соответствовали конструкторам класса Solution.
*/

public class Solution {

    public Solution(String n){}
    public Solution(int a){}
    public Solution(){}

    private Solution(String a, String b){}
    private Solution(int a, int b){}
    private Solution(double d){}

    protected Solution(String a, String b, String c){}
    protected Solution(int a, int b, int c){}
    protected Solution(short a){}

    Solution(String a, String b, String c, String d){}
    Solution(int a, int b, int c, int d){}
    Solution(long l){}
}

