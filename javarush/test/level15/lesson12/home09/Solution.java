package com.javarush.test.level15.lesson12.home09;

/* Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            String url = reader.readLine();

            String s = url.substring(url.indexOf("?")+1);

            String parts[] = s.split("&");

            for (String x : parts) {
                String a = null;

                if (x.indexOf("=") != -1) { a = x.substring(0, x.indexOf("="));}
                else a=x;

                System.out.print(a + " ");

            }
            System.out.println();


                for (String x : parts) {

                        if (x.startsWith("obj=")) {
                            String i = x.substring(x.indexOf("obj") + 4);
                            try {
                                double i2 = Double.parseDouble(i);
                                alert(i2);
                                } catch (NumberFormatException e) {
                                String i2 = i;
                                alert(i2);
                                                                  }
                                                }
                                      }
        }
        catch (IOException e){}

    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}
