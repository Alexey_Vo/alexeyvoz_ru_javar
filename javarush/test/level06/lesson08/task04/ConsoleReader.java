package com.javarush.test.level06.lesson08.task04;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Класс ConsoleReader
Сделать класс ConsoleReader, у которого будут 4 статических метода:
String readString() – читает с клавиатуры строку
int readInt() – читает с клавиатуры число
double readDouble() – читает с клавиатуры дробное число
void readLn() – ждет нажатия enter [использовать readString()]
*/

public class ConsoleReader
{
    public static String readString() throws Exception
    {
        BufferedReader red = new BufferedReader(new InputStreamReader(System.in));
        String a = red.readLine();
        return a;

    }

    public static int readInt() throws Exception
    {
        BufferedReader red = new BufferedReader(new InputStreamReader(System.in));
        int a =Integer.parseInt(red.readLine());
        return a;
    }

    public static double readDouble() throws Exception
    {
        BufferedReader red = new BufferedReader(new InputStreamReader(System.in));
        double a = Double.parseDouble(red.readLine());
        return a;

    }

    public static void readLn() throws Exception
    {
        readString();

    }
}
