package com.javarush.test.level20.lesson10.home03;

import java.io.*;

/* Найти ошибки
Почему-то при сериализации/десериализации объекта класса B возникают ошибки.
Найдите проблему и исправьте ее.
Класс A не должен реализовывать интерфейсы Serializable и Externalizable.
Сигнатура класса В не содержит ошибку :)
*/
public class Solution implements Serializable {
    public static class A {
        public A() {}
        protected String name = "A";
        public A(String name) {
            this.name += name;
        }
    }

    public class B extends A implements Serializable {
        public B(String name) {
            super(name);
            this.name += name;
        }
        private void writeObject(ObjectOutputStream out) throws IOException{
            out.writeObject(this.name);
        }
        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException{
           name = (String) in.readObject();
        }
    }

    public static void main(String[] args) throws ClassNotFoundException, IOException {
        FileOutputStream fileOutput = new FileOutputStream("D:\\1.txt");
        ObjectOutputStream outputStream = new ObjectOutputStream(fileOutput);
        Solution sol = new Solution();
        Solution.B b = sol.new B("Go");
        System.out.println(b.name);
        outputStream.writeObject(b);


        fileOutput.close();
        outputStream.close();


        FileInputStream fiStream = new FileInputStream("D:\\1.txt");
        ObjectInputStream objectStream = new ObjectInputStream(fiStream);

        B loadedb = (B) objectStream.readObject();

        System.out.println(loadedb.name);
        fiStream.close();
        objectStream.close();
    }

}
