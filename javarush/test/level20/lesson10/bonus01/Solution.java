package com.javarush.test.level20.lesson10.bonus01;

import java.util.ArrayList;
import java.util.TreeSet;

/* Алгоритмы-числа
Число S состоит из M чисел, например, S=370 и M(количество цифр)=3
Реализовать логику метода getNumbers, который должен среди натуральных чисел меньше N (long)
находить все числа, удовлетворяющие следующему критерию:
число S равно сумме его цифр, возведенных в M степень
getNumbers должен возвращать все такие числа в порядке возрастания

Пример искомого числа:
370 = 3*3*3 + 7*7*7 + 0*0*0
8208 = 8*8*8*8 + 2*2*2*2 + 0*0*0*0 + 8*8*8*8

На выполнение дается 10 секунд и 50 МБ памяти.
*/
public class Solution {
    public static int[] getNumbers(long N) {
        int[] result = new int[1000];
        int count = 0;
        int sum = 0;
        for (int i = 1; i < N; i++) {
            String[] a = String.valueOf(i).split("");

            for (String s :  a) {
                sum = sum +  (int)Math.pow(Integer.parseInt(s), a.length); }
            if(i == sum) { result[count] = i;
                    count++;}
            sum = 0;

        }

        return result;
    }



    public static void main(String[] args) {
        Long t0 = System.currentTimeMillis();
        long n = (long) Math.pow(10, 8);
        int[] numbers = getNumbers(n);
        Long t1 = System.currentTimeMillis();
        System.out.println("time: " + (t1 - t0) / 1000d + " sec");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + ", ");
        }
            }
}
