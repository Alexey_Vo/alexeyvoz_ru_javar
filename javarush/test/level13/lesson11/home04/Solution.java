package com.javarush.test.level13.lesson11.home04;

/* Запись в файл
1. Прочесть с консоли имя файла.
2. Считывать строки с консоли, пока пользователь не введет строку "exit".
3. Вывести все строки в файл, каждую строчку с новой стороки.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        FileWriter output = new FileWriter(filename);


        String data = reader.readLine();

        while (!data.equals("exit")){
            data = data + "\r\n";
        output.write(data);


            data = reader.readLine();
        }
        output.write("exit\r\n");
        output.close();

    }
}
