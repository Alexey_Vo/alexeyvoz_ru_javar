package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        BufferedReader readerfromFile = new BufferedReader(new FileReader(filename));

        ArrayList<Integer> list = new ArrayList<Integer>();


        while (readerfromFile.ready()){
            int data = Integer.parseInt(readerfromFile.readLine());

            if(data%2==0) list.add(data);

        }
        Collections.sort(list);

        for (int x : list){
            System.out.println(x);
    }

        readerfromFile.close();
        reader.close();
    }

}
