package com.javarush.test.level24.lesson02.home01;

/**
 * Created by User on 19.09.2015.
 */
public class SelfInterfaceMarkerImpl implements SelfInterfaceMarker {
    public SelfInterfaceMarkerImpl() {
    }
    public void m1(){
        System.out.println("metod m1");
    }
    public void m2(){
        System.out.println("metod m2");
    }
}
