package com.javarush.test.level29.lesson15.big01.human;

/**
 * Created by User on 31.10.2016.
 */
public class Soldier extends Human{

    protected int course;

    public Soldier(String name, int age) {
        super(name, age);
    }

    @Override
    public void live() {
        fight();
    }
    public void fight(){}
}
