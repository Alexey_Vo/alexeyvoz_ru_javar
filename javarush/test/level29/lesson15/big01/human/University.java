package com.javarush.test.level29.lesson15.big01.human;

import java.util.ArrayList;
import java.util.List;

public class University {

    private List<Student> students = new ArrayList<>();
    String name;
    int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public University(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student getStudentWithAverageGrade(double averageGrade) {
        //TODO:
        for (Student x :
                students) {
            if (x.getAverageGrade() == averageGrade) return x;

        }
        return null;

    }

    public Student getStudentWithMaxAverageGrade() {
        //TODO:
        double temp = 0;
        Student maxStud = students.get(0);
        for (Student x :
                students) {
            if (x.getAverageGrade() > temp) {
                maxStud =x;
                temp = x.getAverageGrade();
            }
        }
        return maxStud;
    }

    public Student getStudentWithMinAverageGrade() {
        //TODO:
        double temp = 100;
        Student minStud = students.get(0);
        for (Student x :
                students) {
            if (x.getAverageGrade() < temp) {
                minStud = x;
                temp = x.getAverageGrade();
            }
        }
        return minStud;
    }

    public void expel(Student student){
        students.remove(student);
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<Student> getStudents() {
        return students;
    }
}
