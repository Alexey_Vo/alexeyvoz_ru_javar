package com.javarush.test.level21.lesson16.big01;


import java.util.ArrayList;

/**
 * Created by User on 24.08.2015.
 */
public class Hippodrome {
    public static Hippodrome game;



    public static void main(String[] args) throws InterruptedException{
        game = new Hippodrome();
        game.getHorses().add(new Horse("Anna", 3, 0));
        game.getHorses().add(new Horse("Mary", 3, 0));
        game.getHorses().add(new Horse("Mazy", 3, 0));
        game.run();
        game.printWinner();
    }

    public ArrayList<Horse> horses = new ArrayList<>();

    public ArrayList<Horse> getHorses() {
        return horses;
    }
    public void run() throws InterruptedException{
        for (int i = 1; i <= 100 ; i++) {
            move();
            print();
            Thread.sleep(300);
        }
    }
    public void move(){
        for (Horse x : horses) x.move();
    }
    public void print(){
        System.out.println("");
        for (Horse x : horses) x.print();

        System.out.println("");
    }

    public Horse getWinner(){
        double temp = -1;
        Horse win = null;
        for (int i = 0; i < horses.size(); i++) {
            if(horses.get(i).getDistance()>temp) {temp = horses.get(i).getDistance(); win = horses.get(i);}
        }
        return win;
    }
    public void printWinner(){
        System.out.println("Winner is "+getWinner().getName()+"!");
    }

}
