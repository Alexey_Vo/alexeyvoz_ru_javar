package com.javarush.test.level22.lesson09.task03;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* Составить цепочку слов
В методе main считайте с консоли имя файла, который содержит слова, разделенные пробелом.
В методе getLine используя StringBuilder расставить все слова в таком порядке,
чтобы последняя буква данного слова совпадала с первой буквой следующего не учитывая регистр.
Каждое слово должно участвовать 1 раз.
Метод getLine должен возвращать любой вариант.
Слова разделять пробелом.
В файле не обязательно будет много слов.

Пример тела входного файла:
Киев Нью-Йорк Амстердам Вена Мельбурн

Результат:
Амстердам Мельбурн Нью-Йорк Киев Вена
*/
public class Solution {
    public static void main(String[] args) throws IOException{
        //...
        List<String> words = new ArrayList<>();
        BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fileReader = new BufferedReader(new FileReader(reader.readLine()));
        reader.close();
        while (fileReader.ready())
            words.addAll(Arrays.asList(fileReader.readLine().split(" ")));
        fileReader.close();
        String[] wordsA = new String[words.size()];
        wordsA = words.toArray(wordsA);

       StringBuilder result = getLine(wordsA);
       System.out.println(result.toString());
    }

   public static StringBuilder getLine(String... words) {
       String[] ishod = new String[words.length];
       for (int i = 0; i < words.length - 1; i++) {
           ishod[i] =words[i];
       }
       if(words == null || words.length == 0) return new StringBuilder();
        StringBuilder temp = new StringBuilder(words[0].toLowerCase()).delete(1, words[0].length());
        StringBuilder result = new StringBuilder();

        int i =0, j = 0;

       do{
if(words[i] != null) {
    StringBuilder first = new StringBuilder(words[i].toLowerCase()).delete(1, words[i].length());
    StringBuilder last = new StringBuilder(words[i].toLowerCase()).reverse().delete(1, words[i].length());


           if(first.toString().equals(temp.toString()))
           {
               result.append(words[i]+ " ");

               words[i]= null;
               i++;
               j++;
               temp = last;
           }
           else {i++;j++;}

           if(j==words.length*31) {
               result = new StringBuilder();
               for (int k = 0; k < ishod.length - 1; k++) {
                   words[k]=ishod[k];

               }
               temp = new StringBuilder(words[i].toLowerCase()).delete(1, words[i].length());
               j = 0;
               }


       }
        else  i++;
           if(i==words.length)
               i = 0;
       }
       while(result.toString().split(" ").length!= words.length);

        return result.delete(result.length()-1, result.length());
    }

}
