package com.javarush.test.level22.lesson05.task01;

/* Найти подстроку
Метод getPartOfString должен возвращать подстроку начиная с символа после 1-го пробела и до конца слова,
которое следует после 4-го пробела.
Пример: "JavaRush - лучший сервис обучения Java."
Результат: "- лучший сервис обучения"
На некорректные данные бросить исключение TooShortStringException (сделать исключением).
Сигнатуру метода getPartOfString не менять.
*/
public class Solution {
    public static String getPartOfString(String string) throws TooShortStringException{
        try {
            int fistsSpace = string.indexOf(" ");
            int sp = fistsSpace;
            for (int i = 2; i <= 4; i++) {
                sp = string.indexOf(" ", sp+1);
            }
            int fourSpace = string.indexOf(" ", sp+1);
            String result = string.substring(fistsSpace+1, fourSpace);

            return result;
        }
        catch (Exception e){throw new TooShortStringException();}

    }

    public static class TooShortStringException extends Exception {
    }


}
