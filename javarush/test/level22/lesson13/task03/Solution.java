package com.javarush.test.level22.lesson13.task03;

/* Проверка номера телефона
Метод checkTelNumber должен проверять, является ли аргумент telNumber валидным номером телефона.
Критерии валидности:
1) если номер начинается с '+', то он содержит 12 цифр
2) если номер начинается с цифры или открывающей скобки, то он содержит 10 цифр
3) может содержать 0-2 знаков '-', которые не могут идти подряд
4) может содержать 1 пару скобок '(' и ')'  , причем если она есть, то она расположена левее знаков '-'
5) скобки внутри содержат четко 3 цифры
6) номер не содержит букв
7) номер заканчивается на цифру

Примеры:
+380501234567 - true
+38(050)1234567 - true
+38050123-45-67 - true
050123-4567 - true

+38)050(1234567 - false
+38(050)1-23-45-6-7 - false
050ххх4567 - false
050123456 - false
(0)501234567 - false
*/
public class Solution {

    public static boolean checkTelNumber(String telNumber) {

        /*char[] str = telNumber.toCharArray();
        String[] spl = telNumber.split("-");
        String temp = telNumber.replaceAll("\\W", "");

        boolean tire3_1 = true;
        for (String s : spl) {
            if (s.equals("")) tire3_1 = false;
        }
        boolean tire3 = telNumber.split("-").length <= 3 && tire3_1;

        boolean skob45 = spl[0].contains("(") && spl[0].contains(")") && spl[0].indexOf("(") == spl[0].lastIndexOf("(") && spl[0].indexOf("(") == spl[0].lastIndexOf("(") && (spl[0].indexOf("(") - spl[0].indexOf(")") == -4) && spl[0].indexOf("(") < spl[0].indexOf(")");
        boolean skob45_1 = telNumber.contains("(") || telNumber.contains(")");
        boolean plus1 = new String(str, 0, 1).equals("+") && (telNumber.replaceAll("\\D", "").length() == 12);
        boolean first2 = false;
        if (new String(str, 0, 1).equals("(") || Character.isDigit(str[0]))
            if (telNumber.replaceAll("\\D", "").length() == 10) first2 = true;


        if (Character.isDigit(str[str.length - 1]) && !(temp.replaceAll("\\d", "").length() > 0))  // не содержит букв и последняя цифра
        {

            if (plus1 || first2) {

                if ((telNumber.contains("(") || telNumber.contains(")")) && telNumber.contains("-")) {
                    if (skob45 && tire3) {
                        System.out.println("ya");return true;}
                    else return false;
                } else {
                    if (telNumber.contains("-")) {

                        if (tire3) {
                            return true;
                        }
                        else return false;
                    }

                    if (telNumber.contains("(") || telNumber.contains(")")) {
                        if (skob45) return true;
                        else return false;
                    }
                    return true;
                }


            } else return false;

        }
        return false;
*/


       /* if(telNumber.matches("^\\+{1}\\d{12}")) return true;//1
        if(telNumber.matches("^\\d{10}|\\(\\d{10}\\d$"))return true; //2 + 7
        if(telNumber.matches("(\\(\\d{3}\\)){1}.*-")) // скобки есть 1 и внутри них 3 цифры и левее тире
        ([^-]*-[^-]*){0,2} // От нуля до 2х тире и не рядом  3
        */
        if(telNumber.matches("^\\+?\\d*\\(?\\d{3}\\)?\\d+-?\\d+-?\\d+$")) return true;
        else return false;


    }



    public static void main(String[] args) {

      System.out.println(checkTelNumber("+380501234567")); //true
        System.out.println(checkTelNumber("+38(050)1234567"));//true
        System.out.println(checkTelNumber("+38050123-45-67"));//true
         System.out.println(checkTelNumber("050123-4567"));//false

        System.out.println(checkTelNumber("+38)050(1234567")); //false

        System.out.println(checkTelNumber("+38(050)1-23-45-6-7")); //false

        System.out.println(checkTelNumber("050ххх4567"));//false
        System.out.println(checkTelNumber("050123456"));
        System.out.println(checkTelNumber("(0)501234567"));


    }
}
