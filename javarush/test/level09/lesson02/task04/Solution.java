package com.javarush.test.level09.lesson02.task04;

/* Стек-трейс длиной 10 вызовов
Напиши код, чтобы получить стек-трейс длиной 10 вызовов.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        method1();

    }

    public static StackTraceElement[] method1()
    {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println(stackTraceElements[2]);
        return method2();
    }

    public static StackTraceElement[] method2()
    {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println(stackTraceElements[2]);

        return method3();

    }

    public static StackTraceElement[] method3()
    {

        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println(stackTraceElements[2]);
        return method4();//Напишите тут ваш код

    }

    public static StackTraceElement[] method4()
    {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println(stackTraceElements[2]);

        return method5(); //Напишите тут ваш код

    }

    public static StackTraceElement[] method5()
    {

        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println(stackTraceElements[2]);
        return method6(); //Напишите тут ваш код

    }

    public static StackTraceElement[] method6()
    {

        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println(stackTraceElements[2]);
        return method7();  //Напишите тут ваш код

    }

    public static StackTraceElement[] method7()
    {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println(stackTraceElements[2]);

        return method8(); //Напишите тут ваш код

    }

    public static StackTraceElement[] method8()
    {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println(stackTraceElements[2]);

        return method9(); //Напишите тут ваш код

    }

    public static StackTraceElement[] method9()
    {StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println(stackTraceElements[2]);

        return method10();
    }

    public static StackTraceElement[] method10()
    {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println(stackTraceElements[2]);
        return Thread.currentThread().getStackTrace();
    }
}


