package com.javarush.test.level09.lesson02.task02;

/* И снова StackTrace
Написать пять методов, которые вызывают друг друга. Каждый метод должен возвращать имя метода, вызвавшего его, полученное с помощью StackTrace.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {

        method1();
    }

    public static String method1()
    {

        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        StackTraceElement element = stackTraceElements[2];
        String name = element.getMethodName();
        method2();
        return name;

    }

    public static String method2()
    {

        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        StackTraceElement element = stackTraceElements[2];
        String name= element.getMethodName();
        method3();
        return name;

    }

    public static String method3()
    {

        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        StackTraceElement element = stackTraceElements[2];
        String name = element.getMethodName();
        method4();
        return name;
//Напишите тут ваш код

    }

    public static String method4()
    {

        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        StackTraceElement element = stackTraceElements[2];
        String name = element.getMethodName();
        method5();
        return name;
        //Напишите тут ваш код

    }

    public static String method5()
    {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        StackTraceElement element = stackTraceElements[2];
        String name = element.getMethodName();

        return name;
//Напишите тут ваш код

    }
}
