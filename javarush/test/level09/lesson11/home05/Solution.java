package com.javarush.test.level09.lesson11.home05;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* Гласные и согласные буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа должна вывести на экран две строки:
1. первая строка содержит только гласные буквы
2. вторая - только согласные буквы и знаки препинания из введённой строки.
Буквы соединять пробелом.

Пример ввода:
Мама мыла раму.
Пример вывода:
а а ы а а у
М м м л р м .
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String li = reader.readLine();
        char[] buf = li.toCharArray();

        ArrayList<String> glas = new ArrayList<String>();
        ArrayList<String> other = new ArrayList<String>();




        for (char a : buf) {
         if(isVowel(a)) glas.add(String.valueOf(a));
            else {
             if(String.valueOf(a).equals(" ")) continue;
             else other.add(String.valueOf(a));
         }
        }
            for (String g : glas) System.out.print(g+" ");
            System.out.println();
            for (String o: other) System.out.print(o+" ");

            }


    public static char[] vowels = new char[]{'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};

    //метод проверяет, гласная ли буква
    public static boolean isVowel(char c)
    {
        c = Character.toLowerCase(c);  //приводим символ в нижний регистр - от заглавных к строчным буквам

        for (char d : vowels)   //ищем среди массива гласных
        {
            if (c == d)
                return true;
        }
        return false;
    }
}
