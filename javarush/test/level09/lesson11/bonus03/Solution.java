package com.javarush.test.level09.lesson11.bonus03;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* Задача по алгоритмам
Задача: Пользователь вводит с клавиатуры список слов (и чисел). Слова вывести в возрастающем порядке, числа - в убывающем.
Пример ввода:
Вишня
1
Боб
3
Яблоко
2
0
Арбуз
Пример вывода:
Арбуз
3
Боб
2
Вишня
1
0
Яблоко
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<String>();
        while (true)
        {
            String s = reader.readLine();
            if (s.isEmpty()) break;
            list.add(s);
        }

        String[] array = list.toArray(new String[list.size()]);
        sort(array);

        for (String x : array)
        {
            System.out.println(x);
        }
    }

    public static void sort(String[] array) {


        ArrayList<String> listInt= new ArrayList<String>();
        ArrayList<String> listStr= new ArrayList<String>();

        int countI = 0;
        int countSt = 0;
        String temps;

        for (String x : array){
            if( isNumber(x)) listInt.add(x);
            else listStr.add(x);
        }

        String [] arInt = listInt.toArray(new String[listInt.size()]);
        String [] arStr = listStr.toArray(new String[listStr.size()]);

        for (int i = 0; i < listInt.size(); i++) {
            for (int j = i; j < arInt.length; j++) {
                if(Integer.parseInt(arInt[i])<Integer.parseInt(arInt[j])){
                    temps = arInt[i];
                    arInt[i] = arInt[j];
                    arInt[j] = temps;
                }
            }
        }

        for(int i=0; i<arStr.length; i++) {
            for (int j = i; j < arStr.length; j++) {

                if (isGreaterThen(arStr[i], arStr[j])) {
                    temps = arStr[i];
                    arStr[i] = arStr[j];
                    arStr[j] = temps;
                }
            }
        }

        for(int i = 0; i < array.length; i++){
            if(isNumber(array[i])) {array[i] = arInt[countI]; countI++;}
            else{array[i] = arStr[countSt]; countSt++;}
        }

        }

    //Метод для сравнения строк: 'а' больше чем 'b'
    public static boolean isGreaterThen(String a, String b)
    {
        return a.compareTo(b) > 0;
    }


    //строка - это на самом деле число?
    public static boolean isNumber(String s)
    {
        if (s.length() == 0) return false;

        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++)
        {
            char c = chars[i];
            if ((i != 0 && c == '-') //есть '-' внутри строки
                    || (!Character.isDigit(c) && c != '-') ) // не цифра и не начинается с '-'
            {
                return false;
            }
        }
        return true;
    }
}
