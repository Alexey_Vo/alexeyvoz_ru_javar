package com.javarush.test.level07.lesson04.task03;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 2 массива
1. Создать массив на 10 строк.
2. Создать массив на 10 чисел.
3. Ввести с клавиатуры 10 строк, заполнить ими массив строк.
4. В каждую ячейку массива чисел записать длину строки из массива строк, индекс/номер ячейки которой совпадает с текущим индексом из массива чисел. Вывести содержимое массива чисел на экран, каждое значение выводить с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //String s = "1234";
        //System.out.println(s.length());

        String [] arraySt = new String[10];
        int [] arrayInt = new int[10];

        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < arraySt.length; i++) {
            arraySt[i]= r.readLine();
        }
        for (int j = 0; j < arrayInt.length; j++) {
            arrayInt[j]=arraySt[j].length();
        }
        for (int k = 0; k < arrayInt.length; k++) {
            System.out.println(arrayInt[k]);

        }
    }
}
