package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human). Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        Human ded = new Human();
        Human ded2 = new Human();
        Human baba = new Human();
        Human baba2 = new Human();
        Human fat = new Human();
        Human mot = new Human();
        Human chil1 = new Human();
        Human chil2 = new Human();
        Human chil3 = new Human();

        ded.name = "Vasya";
        ded.sex = true;
        ded.age = 90;

        ded2.name = "Vasy";
        ded2.sex = true;
        ded2.age = 90;


       baba.name = "Marfa";
        baba.sex = false;
        baba.age = 87;

        baba2.name = "Marf";
        baba2.sex = false;
        baba2.age = 87;

        fat.name = "Igor";
        fat.sex = true;
        fat.age = 38;
        fat.father = ded;
        fat.mother = baba;

        mot.name = "Alena";
        mot.sex = false;
        mot.age = 30;
        mot.father = ded2;
        mot.mother = baba2;

        chil1.name = "Arsen";
        chil1.sex = true;
        chil1.age = 11;
        chil1.father = fat;
        chil1.mother = mot;

        chil2.name = "Zulya";
        chil2.sex = false;
        chil2.age = 9;
        chil2.father = fat;
        chil2.mother = mot;

        chil3.name = "Ashot";
        chil3.sex = true;
        chil3.age = 3;
        chil3.father = fat;
        chil3.mother = mot;



        System.out.println(ded.toString());
        System.out.println(ded2.toString());
        System.out.println(baba.toString());
        System.out.println(baba2.toString());
        System.out.println(fat.toString());
        System.out.println(mot.toString());
        System.out.println(chil1.toString());
        System.out.println(chil2.toString());
        System.out.println(chil3.toString());



    }

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}
