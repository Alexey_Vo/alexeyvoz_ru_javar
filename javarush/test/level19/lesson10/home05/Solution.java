package com.javarush.test.level19.lesson10.home05;

/* Слова с цифрами
В метод main первым параметром приходит имя файла1, вторым - файла2.
Файл1 содержит слова, разделенные пробелом.
Записать через пробел в Файл2 все слова, которые содержат цифры, например, а1 или abc3d
Закрыть потоки
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws FileNotFoundException, IOException{
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        FileWriter fw = new FileWriter(args[1]);
        String s = "";
        while (reader.ready()){
            s = s+" "+reader.readLine();

        }
        String[] line = s.split(" ");
        for(String x : line){

            if(x.replaceAll("\\d", "ЕСТЬ").contains("ЕСТЬ")) {fw.append(x); fw.append(" ");}
        }



        reader.close();
        fw.close();
    }
}
