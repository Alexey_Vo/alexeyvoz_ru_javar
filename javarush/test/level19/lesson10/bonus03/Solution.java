package com.javarush.test.level19.lesson10.bonus03;

/* Знакомство с тегами
Считайте с консоли имя файла, который имеет HTML-формат
Пример:
Info about Leela <span xml:lang="en" lang="en"><b><span>Turanga Leela
</span></b></span>
Первым параметром в метод main приходит тег. Например, "span"
Вывести на консоль все теги, которые соответствуют заданному тегу
Каждый тег на новой строке, порядок должен соответствовать порядку следования в файле
Количество пробелов, \n, \r не влияют на результат
Файл не содержит тег CDATA, для всех открывающих тегов имеется отдельный закрывающий тег, одиночных тегов нету
Тег может содержать вложенные теги
Пример вывода:
<span xml:lang="en" lang="en"><b><span>Turanga Leela</span></b></span>
<span>Turanga Leela</span>

Шаблон тега:
<tag>text1</tag>
<tag text2>text1</tag>
<tag
text2>text1</tag>

text1, text2 могут быть пустыми
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {

    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        BufferedReader fr = new BufferedReader(new FileReader(filename));
        String s = "";
         String tg = args[0];
        while (fr.ready()){
            s = s + fr.readLine()+" ";
        }
        s = s.substring(0, s.length()-1);


        s = s.replaceAll("\\s", "");
        tagpars(s, tg);


        reader.close();
        fr.close();


    }
    public static void tagpars(String s, String a){
        int a1 = s.indexOf("<" +a);
        int a2 = s.lastIndexOf("</"+a+">")+3+a.length();

        s = s.substring(a1, a2);
        System.out.println(s);
        s = s.substring(s.indexOf("<" +a)+1+a.length(), s.lastIndexOf("</"+a+">"));

        if(s.contains("<" +a)) tagpars(s,a );

    }
//использовать найденые начала открытия тэгов, но уже в строке, где не вырезал пробелы и тд
    //проверить нынещний код на работспособность, добавить тэгов

}
