package com.javarush.test.level19.lesson05.task05;

/* Пунктуация
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Удалить все знаки пунктуации, включая символы новой строки. Результат вывести во второй файл.
http://ru.wikipedia.org/wiki/%D0%9F%D1%83%D0%BD%D0%BA%D1%82%D1%83%D0%B0%D1%86%D0%B8%D1%8F
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        String fileName_1;
        String fileName_2;
        String line;
        String line_sum = "";

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        fileName_1 = reader.readLine();
        fileName_2 = reader.readLine();

        File file_1 = new File(fileName_1);
        File file_2 = new File(fileName_2);
        BufferedReader fin = new BufferedReader(new FileReader(file_1));


    int symbol = fin.read();
    while (symbol != -1) {

        line_sum = line_sum + (char)symbol;
        symbol = fin.read();

    }

        line_sum = line_sum.replaceAll("\\s|\\W", "");

        FileWriter fr = null;
        try {
            fr = new FileWriter(file_2);
            fr.write(line_sum);
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        reader.close();
        fin.close();

    }
}
