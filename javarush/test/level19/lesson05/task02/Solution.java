package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть поток ввода.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        FileReader fr = new FileReader(filename);
char[] buff = new char[1024];

        while (fr.ready()){
            fr.read(buff);
        }
         String s = "";

        for(char x : buff) s=s+x;

String a = s.replaceAll("\\W", " ");

int count = 0;

        String[] stbuf = a.split(" ");
        for (String x: stbuf){if(x.equals("world")) count++; }
        System.out.println(count);
        reader.close();
        fr.close();
    }
}
