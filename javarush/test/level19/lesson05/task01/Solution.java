package com.javarush.test.level19.lesson05.task01;

/* Четные байты
Считать с консоли 2 имени файла.
Вывести во второй файл все байты с четным индексом.
Пример: второй байт, четвертый байт, шестой байт и т.д.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fn1 = reader.readLine();
        String fn2 = reader.readLine();

        FileReader fr = new FileReader(fn1);
        FileWriter fw = new FileWriter(fn2);
        int a, count=0;
        while ((a = fr.read()) != -1){
            count++;
            if(count%2 == 0) fw.write(a);
        }
        reader.close();
        fr.close();
        fw.close();
    }
}
