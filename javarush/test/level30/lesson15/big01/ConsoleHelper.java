package com.javarush.test.level30.lesson15.big01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User on 13.12.2016.
 */
public class ConsoleHelper {
   private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

   public static void writeMessage(String message){
        System.out.println(message);
    }

    public static String readString(){
        String s = "";
        while (true){
            try {
                s = bufferedReader.readLine();
                break;
            } catch (IOException e) {
                System.out.println("Произошла ошибка при попытке ввода текста. Попробуйте еще раз.");
            }
        }

        return s;
    }


    public static int readInt(){
        int i = 0;
        while (true){
            try {
                i = Integer.parseInt(readString());

                break;
            } catch (NumberFormatException e) {
                System.out.println("Произошла ошибка при попытке ввода числа. Попробуйте еще раз.");
            }
        }

        return i;
    }
}
