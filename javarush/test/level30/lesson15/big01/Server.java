package com.javarush.test.level30.lesson15.big01;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by User on 13.12.2016.
 */
public class Server {

    private static Map<String, Connection> connectionMap = new ConcurrentHashMap<String, Connection>();



    public static void sendBroadcastMessage(Message message){
try {
    for (Map.Entry<String, Connection> entry : Server.connectionMap.entrySet()) {
        entry.getValue().send(message);
    }
}
catch (IOException ex){
    ConsoleHelper.writeMessage("Не удалось отправить сообщение.");
}
    }


    private static class Handler extends Thread {

        private Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }


        public void run() {
            String name = null;
            try (Connection connection = new Connection(socket)) {
                ConsoleHelper.writeMessage(String.format("New connection established from %s ", connection.getRemoteSocketAddress()));
                name = serverHandshake(connection);
                sendBroadcastMessage(new Message(MessageType.USER_ADDED, name));
                sendListOfUsers(connection, name);
                serverMainLoop(connection, name);
            } catch (IOException | ClassNotFoundException e) {
                ConsoleHelper.writeMessage("Communication error occurred");
            }
            if (name != null) {
                connectionMap.remove(name);
                sendBroadcastMessage(new Message(MessageType.USER_REMOVED, name));
            }
            ConsoleHelper.writeMessage("Connection closed");

            ConsoleHelper.writeMessage("Укажите порт сервера :");
            ServerSocket serverSocket = null;
            try {
                serverSocket = new ServerSocket(ConsoleHelper.readInt());
                ConsoleHelper.writeMessage("Сервер запущен");
                while (true) {
                    Socket socket = serverSocket.accept();

                    Handler handler = new Handler(socket);
                    handler.start();
                }
            } catch (IOException e) {
                e.printStackTrace();

            } finally {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
                    private String serverHandshake(Connection connection) throws IOException, ClassNotFoundException {
                        while (true) {
                            connection.send(new Message(MessageType.NAME_REQUEST));
                            Message message = connection.receive();
                            if (message.getType() == MessageType.USER_NAME) {
                                String userName = message.getData();
                                if (userName != "" && userName != null && !userName.isEmpty() && !connectionMap.containsKey(userName)) {
                                    connectionMap.put(userName, connection);
                                    connection.send(new Message(MessageType.NAME_ACCEPTED));
                                    return userName;
                                }
                            }
                        }
                    }

                    private void sendListOfUsers(Connection connection, String userName) throws IOException{
                        for (Map.Entry<String, Connection> entry : Server.connectionMap.entrySet()) {
                            String name = entry.getKey();
                            Message message = new Message(MessageType.USER_ADDED, name);
                            if(userName != name) connection.send(message);


                        }
                    }



                    private void serverMainLoop(Connection connection, String userName) throws IOException, ClassNotFoundException{
                        while (true){
                            Message message = connection.receive();
                            if(message.getType() == MessageType.TEXT) sendBroadcastMessage(new Message(MessageType.TEXT, userName+": "+ message.getData()));
                            else ConsoleHelper.writeMessage("Ошибка");}

                    }


    }

    public static void main(String[] args){
        ConsoleHelper.writeMessage("Введите порт сервера");
        try (ServerSocket serverSocket = new ServerSocket(ConsoleHelper.readInt())) {
            ConsoleHelper.writeMessage("Сервер запущен");
            while (true) {
                new Handler(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            ConsoleHelper.writeMessage("Ошибка сокета сервера");
        }

    }

}