package com.javarush.test.level30.lesson15.big01.client;

import com.javarush.test.level30.lesson15.big01.*;

import java.io.IOException;
import java.net.Socket;


/**
 * Created by User on 27.12.2016.
 */
public class Client {
    protected Connection connection;
    private volatile boolean clientConnected = false;


    public class SocketThread extends Thread{

        public void run(){
           String serverAdress = getServerAddress();
           int serverPort = getServerPort();
            Socket socket = null;


            try {
                socket = new Socket(serverAdress, serverPort);
                connection = new Connection(socket);
                clientHandshake();
                clientMainLoop();
            } catch (IOException e) {
                notifyConnectionStatusChanged(false);
            } catch (ClassNotFoundException e) {
                notifyConnectionStatusChanged(false);
            }


        }

        protected void processIncomingMessage(String message){
            ConsoleHelper.writeMessage(message);
        }

        protected void informAboutAddingNewUser(String userName){
            ConsoleHelper.writeMessage(userName + " присоединился к чату.");
        }

        protected void informAboutDeletingNewUser(String userName){
            ConsoleHelper.writeMessage(userName + " покинул чат.");
        }

        protected void notifyConnectionStatusChanged(boolean clientConnected){
            Client.this.clientConnected = clientConnected;
            synchronized (Client.this){
                Client.this.notify();
            }
        }

        protected void clientHandshake() throws IOException,ClassNotFoundException{
                while (true){
                   Message message = connection.receive();
                   if(message.getType() == MessageType.NAME_REQUEST) {

                   }
                   switch (message.getType()){
                       case NAME_REQUEST: {
                           String userName = getUserName();
                           connection.send(new Message(MessageType.USER_NAME, userName));
                           break;
                       }
                       case NAME_ACCEPTED:{
                           notifyConnectionStatusChanged(true);
                           return;
                       }
                       default: throw new IOException("Unexpected MessageType");
                   }
                }
        }


        protected void clientMainLoop() throws IOException, ClassNotFoundException{
            while (true){
                Message message = connection.receive();
                switch (message.getType()){
                    case TEXT: processIncomingMessage(message.getData());
                        break;
                    case USER_ADDED: informAboutAddingNewUser(message.getData());
                    break;
                    case USER_REMOVED: informAboutDeletingNewUser(message.getData());
                    break;
                    default: throw new IOException("Unexpected MessageType");
                }
            }
        }

    }

    public static void main(String[] args) {
        Client client = new Client();
        client.run();
    }

    public void run(){


        SocketThread socketThread = getSocketThread();
        socketThread.setDaemon(true);
        socketThread.start();
        try {
            synchronized (this) {
                this.wait();
            }
        } catch (InterruptedException e) {
            ConsoleHelper.writeMessage("Wait error");
            return;
        }

        if(clientConnected) ConsoleHelper.writeMessage("Соединение установлено. Для выхода наберите команду 'exit'.");
        else ConsoleHelper.writeMessage("Произошла ошибка во время работы клиента.");
        while (clientConnected){
            String textMessage = ConsoleHelper.readString();
            if(textMessage.equals("exit")) return;
            else if(shouldSentTextFromConsole()) sendTextMessage(textMessage);
        }
    }


    protected String getServerAddress(){
        ConsoleHelper.writeMessage("Enter adress: ");
        return ConsoleHelper.readString();
    }

    protected int getServerPort(){
        ConsoleHelper.writeMessage("Enter port: ");
        return ConsoleHelper.readInt();
    }

    protected String getUserName(){
        ConsoleHelper.writeMessage("Enter name: ");
        return ConsoleHelper.readString();
    }

    protected boolean shouldSentTextFromConsole(){
        return true;
    }

    protected SocketThread getSocketThread(){
        SocketThread socketThread = new SocketThread();
        return socketThread;
    }

    protected void sendTextMessage(String text){
        try {
            connection.send(new Message(MessageType.TEXT, text));
        } catch (IOException e) {
            ConsoleHelper.writeMessage("error sending");
            clientConnected = false;
        }
    }
}
