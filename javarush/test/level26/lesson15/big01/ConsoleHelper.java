package com.javarush.test.level26.lesson15.big01;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by User on 28.05.2016.
 */
public class ConsoleHelper {

static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


    public static void writeMessage(String message){
        System.out.println(message);
    }

    public static String readString(){
        String s = "";

        try {
           s = br.readLine();
        }
        catch (Exception e){}
        return s;
    }
    public static String askCurrencyCode(){
        System.out.println("Введите код валюты: ");
        String kod = readString();
        if (kod.matches(".{3}")) return kod.toUpperCase();
        else return "Данные некорректны";
    }

    public static String[] getValidTwoDigits(String currencyCode){
        System.out.println("Введите  номинал  и кол-во банкнот: ");
        String cash = "";
        cash = readString();
        String[] strings = cash.split(" ");
        if(strings.length == 2 && Integer.parseInt(strings[0])>0 && Integer.parseInt(strings[1])>0)   return strings;

        else {
            System.out.println("Данные некорректны");
            return getValidTwoDigits(currencyCode);
        }
    }

}
