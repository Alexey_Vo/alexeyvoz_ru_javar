package com.javarush.test.level26.lesson15.big01;

/**
 * Created by User on 28.05.2016.
 */
public enum Operation {
    INFO,
    DEPOSIT,
    WITHDRAW,
    EXIT
}
