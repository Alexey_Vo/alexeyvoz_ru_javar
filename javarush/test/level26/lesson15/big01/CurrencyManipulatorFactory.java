package com.javarush.test.level26.lesson15.big01;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 28.05.2016.
 */
public class CurrencyManipulatorFactory {
    private static Map<String, CurrencyManipulator> curencySklad = new HashMap<>();

    public static final CurrencyManipulatorFactory factory = new CurrencyManipulatorFactory();

    private CurrencyManipulatorFactory() {

    }

    public static CurrencyManipulator getManipulatorByCurrencyCode(String currencyCode){
        CurrencyManipulator manipulator = null;
        if(curencySklad.containsKey(currencyCode)) manipulator = curencySklad.get(currencyCode);
        else {
            manipulator = new CurrencyManipulator(currencyCode);
            curencySklad.put(currencyCode,manipulator);
        }
        return manipulator;
    }
}
