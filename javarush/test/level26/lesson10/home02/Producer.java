package com.javarush.test.level26.lesson10.home02;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by User on 26.05.2016.
 */
public class Producer implements Runnable {
    protected ConcurrentHashMap<String, String> map;

    public Producer(ConcurrentHashMap<String, String> map) {
        this.map = map;
    }

    @Override
    public void run() {
        int i = 1;
        while (true) {
            try {
                System.out.println("Some text for " + i++);
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                System.out.println(Thread.currentThread().getName() + " thread was terminated");
            }
        }
    }
}
