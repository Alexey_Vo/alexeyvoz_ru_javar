package com.javarush.test.level26.lesson02.task01;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import static java.lang.StrictMath.abs;


/* Почитать в инете про медиану выборки
Реализовать логику метода sort, который должен сортировать данные в массиве по удаленности от его медианы
Вернуть отсортированный массив от минимального расстояния до максимального
Если удаленность одинаковая у нескольких чисел, то выводить их в порядке возрастания
*/
public class Solution {
    public static Integer[] sort(Integer[] array) {
        //implement logic here
        final double med ;
        Arrays.sort(array);
        if(array.length%2 == 0 ) med = (array[array.length/2]+array[array.length/2-1])/2.0;
        else med = array[(array.length-1)/2];
        System.out.println(med+ "- med");
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if(abs(o1-med)-abs(o2-med) == 0) return o1-o2  ;
                else return (int) (abs(o1-med)-abs(o2-med));
            }
        };
        Arrays.sort(array, comparator);
        return array;
    }

    public static void main(String[] args) {
        Integer[] arr = new Integer[]{5, 8, 13, 15, 17};
        sort(arr);
        for (int i : arr) System.out.println(i);
    }
}
