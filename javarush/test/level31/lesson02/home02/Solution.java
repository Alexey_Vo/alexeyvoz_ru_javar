package com.javarush.test.level31.lesson02.home02;

import java.io.File;
import java.io.IOException;
import java.util.*;

/* Находим все файлы
Реализовать логику метода getFileTree, который должен в директории root найти список всех файлов включая вложенные.
Используйте очередь, рекурсию не используйте.
Верните список всех путей к найденным файлам, путь к директориям возвращать не надо.
Путь должен быть абсолютный.
*/
public class Solution {
    public static List<String> getFileTree(String root) throws IOException {
        File directory = new File(root);
        ArrayList listOfFiles = new ArrayList<String>();
        Queue<File> queue = new PriorityQueue<>();

        Collections.addAll(queue, directory.listFiles());

        while (!queue.isEmpty()){
            File file = queue.remove();
            if(file.isFile())   listOfFiles.add(file.getAbsolutePath());
            else Collections.addAll(queue, file.listFiles());
        }
        Collections.sort(listOfFiles);
        return listOfFiles;

    }


    public static void main(String[] args) throws IOException {
        String path = "C:/111";
        for (String line :
                getFileTree(path)) {
            System.out.println(line);
        }
    }
}
