package com.javarush.task.task31.task3113;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/* 
Что внутри папки?
*/
public class Solution {

    public static class MyFileVisitor extends SimpleFileVisitor<Path>{

       private List<Path> directories = new ArrayList<>();
       private List<Path> files = new ArrayList<>();

        public List<Path> getDirectories() {
            return directories;
        }

        public List<Path> getFiles() {
            return files;
        }

        public long getSum() {
            return sum;
        }

        private long sum = 0;

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {



            if(!directories.contains(file.getParent())) directories.add(file.getParent());
            files.add(file);
            sum = sum + Files.size(file);
            return FileVisitResult.CONTINUE;
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String stringPathDirectory = reader.readLine();
        Path pathDirectory = Paths.get(stringPathDirectory);
        if(!Files.isDirectory(pathDirectory)) System.out.println(pathDirectory+" - не папка");
        else {
            MyFileVisitor myFileVisitor = new MyFileVisitor();
            Files.walkFileTree(pathDirectory, myFileVisitor);
            System.out.println("Всего папок - "+ (myFileVisitor.getDirectories().size()-1));
            System.out.println("Всего файлов - "+ myFileVisitor.getFiles().size());
            System.out.println("Общий размер - " + myFileVisitor.getSum());


        }
    }
}
