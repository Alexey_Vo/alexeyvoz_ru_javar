package com.javarush.task.task31.task3105;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* 
Добавление файла в архив
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        String fileName = args[0];
        String zipPath = args[1];

        FileInputStream fileInputStream = new FileInputStream(zipPath);
        ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);



        List<ZipEntry> list = new ArrayList<>();
        ZipEntry entry;
        while((entry=zipInputStream.getNextEntry())!=null){
            list.add(entry);
           }
           
        FileOutputStream fileOutputStream = new FileOutputStream(zipPath);
        ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);

        for (ZipEntry entr :
                list) {
            zipOutputStream.putNextEntry(entr);

        }

        File file = new File(fileName);
     //   Files.copy(file.toPath(), zipOutputStream);
        zipInputStream.close();
        zipOutputStream.close();
    }
}
