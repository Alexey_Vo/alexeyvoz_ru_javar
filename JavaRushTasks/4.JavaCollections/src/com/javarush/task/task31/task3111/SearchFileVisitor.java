package com.javarush.task.task31.task3111;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.FileVisitResult.CONTINUE;

/**
 * Created by Byk88 on 09.02.2017.
 */
public class SearchFileVisitor extends SimpleFileVisitor<Path> {

    private List<Path> foundFiles = new ArrayList<>();
    private String partOfName = "";
    private String partOfContent = "";
    private int minSize = Integer.MIN_VALUE ;
    private int maxSize = Integer.MAX_VALUE;

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

        if(file.getFileName().toString().contains(partOfName) && Files.readAllLines(file).toString().contains(partOfContent) && Files.size(file)>minSize && Files.size(file)<maxSize) foundFiles.add(file);



        return CONTINUE;
    }

    public void setPartOfName(String name) {
        this.partOfName = name;
    }

    public void setPartOfContent(String source) {
        this.partOfContent = source;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public List<Path> getFoundFiles() {

        return foundFiles;
    }
}